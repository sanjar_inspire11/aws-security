terraform {
  # --- Creates Organization Level Cloudtrail Resource within Master Account --- #
  #source = "git::git@bitbucket.org:sanjar_inspire11/aws-security.git//modules/cloudtrail"
  source = "../../modules/cloudtrail"
}

include {
  path = find_in_parent_folders()
}

inputs = {
  region = "us-east-1"
  #assume_role_arn   = "arn:aws:iam::xxxxxxxxxxx:role/my-role"
  #role_session_name = "master-cloudtrail-session"

  s3-bucket-name = "my-cloudtrail-logs-bucket"
  s3-bucket-prefix = "my_folder"

  include-include-global-service-events = true
  multi_region_trail = true
  organization_trail = true
  enable-log-file-validation = true

  all_tags = {
    Environment  = "all-envs"
    Application  = "terraform"
    Description  = "cloudtrail"
    Organization = "i11"
    Account      = "master"
  }
}
