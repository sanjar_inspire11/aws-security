# cloudtrail

`(Tue Jun 23 14:26:04 CDT 2020 : Gruntwork does not have module to create Cloudtrail for Organization.)`

Creates **Cloudtrail** resource on Organizational Level. But the variable can be set to create not "organizational" trail as well in case there is need to create per account **Trails**. The current `live` infrastructure includes the **Organizational** level trail recording and logging.

When creating for organizations, this module needs to be created within **master** account, so the `~/.aws/credentials` need to point to **master**.

Logging is being collected at **Logging** account inside **my-cloudtrail-logs-bucket** bucket.

Whenever there is a new AWS Account created, this module needs to be reapplied again so the cloudtrail is enabled on the newly created account.

## Backend
The backend is configured towards **master's S3** bucket and **DynamoDB** table. (`live/terragrunt.hcl` for backend information.)

## Variables
All variables can be found within `modules/variables` directory and any values for variables can be set within `terragrunt.hcl` file like `live/all-envs/terragrunt.hcl`.

`Note! We do not use Environments such as Dev, Stage, Prod since Cloudtrail resource is not environment specific and does not require that.`

## Things to consider
In order to use this module, you will need to have the following:

1. Access to **Master account** and `~/.aws/credentials` pointed towards **master**.
2. Understanding of how `terraform` and `terragrunt` work. In addition, installed and configured on the workstation.
3. Understanding of how **Cloudtrail** works.
4. Access to **Logging** account to ensure that Cloudtrail is logging to correct bucket.

# Deploy

1. `cd live/all-envs`
2. Ensure the credentials file is pointed towards **master** account.
3. Run terragrunt commands.

```bash
terragrunt init

terragrunt plan

terragrunt apply

terragrunt state list
```

# Destroy

```bash
# check what is being destroyed
terragrunt plan -destroy

# destroy
terragrunt destroy
```

# Docs
- [Terraform - cloudtrail resource](https://www.terraform.io/docs/providers/aws/r/cloudtrail.html)
- [Creating Organization Trail](https://docs.aws.amazon.com/awscloudtrail/latest/userguide/creating-trail-organization.html)
- [S3 Bucket Policy for Logging bucket](https://docs.aws.amazon.com/awscloudtrail/latest/userguide/create-s3-bucket-policy-for-cloudtrail.html#aggregration-option)
