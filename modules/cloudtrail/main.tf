resource "aws_cloudtrail" "cloudtrail-main" {
  name = "${var.all_tags["Organization"]}-${var.all_tags["Account"]}-${var.all_tags["Application"]}-${var.all_tags["Environment"]}-${var.all_tags["Description"]}"

  s3_bucket_name = var.s3-bucket-name
  s3_key_prefix  = var.s3-bucket-prefix

  include_global_service_events = var.include-include-global-service-events

  is_multi_region_trail = var.multi_region_trail
  is_organization_trail = var.organization_trail

  enable_log_file_validation = var.enable-log-file-validation

  tags = {
    Organization = var.all_tags["Organization"]
    Account      = var.all_tags["Account"]
    Application  = var.all_tags["Application"]
    Environment  = var.all_tags["Environment"]
    Name         = "${var.all_tags["Organization"]}-${var.all_tags["Account"]}-${var.all_tags["Application"]}-${var.all_tags["Environment"]}-${var.all_tags["Description"]}"
    Description  = var.all_tags["Description"]
  }
}
