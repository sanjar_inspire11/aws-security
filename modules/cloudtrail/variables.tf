########################
# PROVIDER INFORMATION #
########################
variable "region" {
  description = "AWS Region to deploy resources to."
}

#variable "assume_role_arn" {
#  description = "Assume role ARN"
#}
#
#variable "role_session_name" {
#  description = "assume role - session name"
#}

#######################
#  Cloudtrail related #
#######################
variable "s3-bucket-name" {
  description = "The bucket name where to deliver the logs."
}

variable "s3-bucket-prefix" {
  description = "The prefix/folder structure to store the logs within S3 Bucket."
}

variable "include-include-global-service-events" {
  description = "Specifies whether the trail is publishing events from global services such as IAM to the log files."
  type        = bool
}

variable "multi_region_trail" {
  description = "Specifies whether the trail is created in the current region or in all regions."
  type        = bool
}

variable "organization_trail" {
  description = "Specifies whether the trail is an AWS Organizations trail. Organization trails log events for the master account and all member accounts. Can only be created in the organization master account."
  type        = bool
}

variable "enable-log-file-validation" {
  description = "Specifies whether log file integrity validation is enabled."
  type        = bool
}

########
# TAGS #
########
variable "all_tags" {
  type        = map(string)
  description = "All other tags"
  #default = {
  #  Application = "my_app_name"
  #  Project     = "my_project_name"
  #  Name        = "automation-workers"
  #  Env         = "my_env"
  #}
}
